// Génération des valeurs

var operande1 = Math.trunc( Math.random() * 10 );
var operande2 = Math.trunc( Math.random() * 10 );

var i = Math.trunc(Math.random()*3);
var operation = "+-*".substring(i, i+1);


// Evénement "quand l'utilisateur saisit une valeur"

var standard_input = process.stdin;
standard_input.setEncoding('utf-8');

console.log("Combien font " + operande1 + operation + operande2 + " :");

standard_input.on('data', function(data){

    var reponse_utilisateur = data;
    var reponse_calculee = 0;

    switch(operation)
    {
        case "*": reponse_calculee = operande1*operande2; break;
        case "+": reponse_calculee = operande1+operande2; break;
        case "-": reponse_calculee = operande1-operande2; break;
    }

    if( reponse_utilisateur==reponse_calculee )
    {
        console.log("Bien calculé !");
    }
    else
    {
        console.log("Raté !");
    }

    process.exit();
});