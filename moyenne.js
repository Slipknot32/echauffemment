var notes = [];



function faire_la_moyenne( tab_notes )
{
    var moyenne = 0;

    for(var i=0 ; i<tab_notes.length ; i++)
    {
        moyenne += tab_notes[i];
    }
    return moyenne / tab_notes.length;
}



var standard_input = process.stdin;
standard_input.setEncoding('utf-8');

standard_input.on('data', function(data){

    notes.push( parseInt(data) );

    console.log(notes);

    if(notes.length>9 )
    {
        var moyenne = faire_la_moyenne(notes);
        console.log("La moyenne est " + moyenne);
        process.exit();
    }
});



