/*
// Echauffement Js 1

var question = "Comment t'appelles-tu ?";

do
{
    var nom = prompt(question);

    if( nom.length<1 )
    {
        alert('Trop court !');
    }
    
    if( nom.length>10 )
    {
        alert('Trop long !');
    }

}
while( nom.length<1 || nom.length>10 );

alert("Bonjour " + nom + " !");


// Echauffement Js 2

while(true)
{
    var couleur_cheval = prompt("De quelle couleur est le cheval blanc d'Henri IV ?");

    if( couleur_cheval.toLowerCase()!="blanc" 
    && couleur_cheval.toLowerCase()!="gris")
    {
        alert("Bidooon !");
    }
    else
    {
        alert("Bien joué pour Henri !");
        break;
    }
}


var nombre_nains = prompt("Combien y-a-t-il de 7 nains ?");


if( nombre_nains.toLowerCase()=="sept" 
|| nombre_nains=="7" )
{
    alert("Bien joué pour les nains !");
}
else
{
    alert("Bidooon !");
}

*/


// Echauffement Js 3

var compteur = 0;
var bouton_connexion = document.getElementById('cnx');

bouton_connexion.addEventListener('click', function(){

    

    

    var identifiant = document.getElementById("login").value;
    var motDePasse = document.getElementById("pass").value;
    
    console.log(identifiant);
    console.log(motDePasse);

    if( compteur>3 )
    {
        alert("Nombre d'essais dépassé !");
    }
    else
    {
        if( identifiant.length<=4 )
        {
            alert("L'identifiant doit être composé de plus de 4 caractères !");
        }
        else
        {
            if( identifiant.indexOf('@') == -1 )
            {
                alert("L'identifiant doit contenir un @ !");
            }
            else
            {
                if( identifiant!="lea@gmail.com" )
                {
                    alert("Mauvais identifiant !");
                }
                else
                {
                    if( motDePasse!="12345" )
                    {
                        compteur++;
                        console.log(compteur);
                        alert("Mot de passe incorrect ( " + compteur+ " essai sur 4)");
                    }
                    else
                    {
                        alert("YES ! ! !");
                    }
                }
            }
        }
    }

});

